from django.urls import path
from django.contrib.auth import views as auth_views

from .views import signup, first, task


app_name = 'core'

urlpatterns = [
    path('signup/', signup, name='signup'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('login/', auth_views.LoginView.as_view(redirect_authenticated_user=True)),
    # path('login/', Login, name='login'),
    path('', first, name='first'),
    path('tasks/', task, name='task'),
    path(r'^tasks/(?P<task_id>\d+)/$', task, name='task'),
    path(r'^tasks/(?P<param>\w+)/$', task, name='task'),
]