from django.db import models
from django.contrib.auth.models import User


# class User:
#     pass


class Task(models.Model):
    title = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    done = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.title} {self.done}'

