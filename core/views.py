from django.shortcuts import render, redirect
from django.views.generic import CreateView
from django.contrib.auth import login, authenticate
from django.contrib.auth.views import LoginView, LogoutView

from .models import User, Task
from .forms import UserCreationForm, TaskForm


def first(request):
    return render(request, 'first.html')




def signup(request):

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(email=email, password=raw_password)
            login(request, user)
            return redirect('/core/')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def task(request, task_id=0, param=None, *args, **kwargs):

    user = request.user
    queryset = Task.objects.filter(user=user)
    task_list = queryset.values('id', 'title', 'done')
    form_task = TaskForm(request.POST)
    if param is not None:
        task_list_dell = Task.objects.filter(user=user, done=True)
        task_list_dell.delete()
        queryset = Task.objects.filter(user=user)
        task_list = queryset.values('id', 'title', 'done')
        return redirect('/core/tasks/')

    if task_id != 0:
        task = Task.objects.get(id=task_id)
        # print(task)
        if task.done is False:
            task.done = True
            task.save()

            return redirect('/core/tasks/')
        else:
            task.done = False
            task.save()
            return redirect('/core/tasks/')

    if request.method == 'GET':
        # print(task_list)
        return render(request, 'task.html', {'tasks': task_list, 'form': TaskForm})
    
    if request.method == 'POST':

        task_list = queryset.values('id', 'title', 'done')
        form_task = TaskForm(request.POST)
        print(task_id)
        if form_task.is_valid():
            # form.save()
            title = form_task.cleaned_data['title']
            user = request.user
            task = Task(title=title, user=user)
            task.save()
            task = None
            return render(request, 'task.html', {'tasks': task_list, 'form': TaskForm})
    else:
        form = TaskForm()
        return render(request, 'task.html', {'form': TaskForm})
