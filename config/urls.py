from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('core/', include('core.urls')),
    # path('core/', include('django.contrib.auth.urls')),
    # path('accounts/profile/', ),
]
